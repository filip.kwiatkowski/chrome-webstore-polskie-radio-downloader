function getProgramLink(document_root) {
    data=JSON.parse(document.querySelector("[data-media]").getAttribute("data-media"))
    if (data.length) {
		url = "https:" + data.file;
		filename = data.title;
		if (filename.length) {
			return "<a href=\"" + url + "\" download=\"" + filename + "\">Pobierz plik z audycją</a>";
		} else {
			return "<a href=\"" + url + "\" download=\"audycja.mp3\">Pobierz plik z audycją</a>";
		}
	}
    return "Nie znalazłem pliku z audycją";
}

chrome.runtime.sendMessage({
    action: "getSource",
    source: getProgramLink(document)
});

